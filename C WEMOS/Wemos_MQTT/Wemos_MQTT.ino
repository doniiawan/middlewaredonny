//import libraries
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <dht.h>

//variables
#define sensor 0
dht DHT;
const char* ssid     = "REST-Middleware";
const char* password = "zxcvbnm123";
const char* mqtt_server = "192.168.1.2";
WiFiClient espClient;
PubSubClient client(espClient);
int sensortemp = 1;
int sensorhum = 1;

 
void callback(char* topic, byte* payload, unsigned int length) {
 Serial.print("Message arrived [");
 Serial.print(topic);
 Serial.print("] ");
 
 String strTopic = String((char*)topic);
 if (strTopic == "node/node2/sensor/led")
   for (int i=0;i<length;i++) {
    char receivedChar = (char)payload[i];
    Serial.print(receivedChar);
    if (receivedChar == '0')
    Serial.print("LED MATI");
    digitalWrite(LED_BUILTIN, HIGH);
    if (receivedChar == '1')
     digitalWrite(LED_BUILTIN, LOW);
     Serial.print("LED HIDUP");
    }
  else if(strTopic == "node/node2/sensor/temperature")
    for (int i=0;i<length;i++) {
    char receivedChar = (char)payload[i];
    Serial.print(receivedChar);
    if (receivedChar == '0')
  
      sensortemp=0;
    if (receivedChar == '1')
      sensortemp=1;
    }
  else if(strTopic == "node/node2/sensor/humidity")
    for (int i=0;i<length;i++) {
    char receivedChar = (char)payload[i];
    Serial.print(receivedChar);
    if (receivedChar == '0')
  
      sensorhum=0;
    if (receivedChar == '1')
      sensorhum=1;
    }
  Serial.println();
}

void reconnect() {
 
 while (!client.connected()) {
 Serial.print("Attempting MQTT connection...");
 
 if (client.connect("WEMOS Client")) {
  Serial.println("connected");
 
  client.subscribe("node/node2/sensor/led");
  client.subscribe("node/node2/sensor/temperature");
  client.subscribe("node/node2/sensor/humidity");
 } else {
  Serial.print("failed, rc=");
  Serial.print(client.state());
  Serial.println(" try again in 5 seconds");
  
  delay(5000);
  }
 }
}

void setup() {
  WiFi.mode(WIFI_STA);
  digitalWrite(LED_BUILTIN, HIGH);
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200);
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print("RECONNECTING");
    Serial.println();
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void loop() {
  if(!client.connected()){
    reconnect();
  }

  client.loop();
  delay(2000);
  int ledstatus = digitalRead(LED_BUILTIN);
  if(ledstatus==1){
    client.publish("node/node2/sensor/led/status","0");  
    
  }else{
    client.publish("node/node2/sensor/led/status","1");  
  }

  DHT.read11(sensor);  
  char temp[50];
  char lembab[50];
  int tempint = int(DHT.temperature);
  int lembabint = int(DHT.humidity);
  String t = String(tempint);
  String h = String(lembabint);
  t.toCharArray(temp,50);
  h.toCharArray(lembab,50);
  if(sensortemp==1){
    client.publish("node/node2/sensor/temperature/status",temp);  
    
  }else{
    client.publish("node/node2/sensor/temperature/status","OFF");  
  }
  if(sensorhum==1){
    client.publish("node/node2/sensor/humidity/status",lembab);  
    
  }else{
    client.publish("node/node2/sensor/humidity/status","OFF");  
  }
}
