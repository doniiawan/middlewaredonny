import time, dht11
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import RPi.GPIO as GPIO

# initialize GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()
LED_PIN = 18
GPIO.setup(LED_PIN, GPIO.OUT)

global sensortemp
global sensorhum
global sensorled

sensortemp = 1
sensorhum = 1
sensorled = 1

Broker = "127.0.0.1"
sub_topic1 = "node/node3/sensor/led"
sub_topic2 = "node/node3/sensor/temperature" # receive messages on this topic


sensordht = dht11.DHT11(pin=4)
        


#MQTT EVENT 

# when connecting to mqtt do this;

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe(sub_topic1)
    client.subscribe(sub_topic2)

# when receiving a mqtt message do this;

def on_message(client, userdata, msg):
    message = str(msg.payload)
    print(msg.topic+" "+message)
    if msg.topic == "node/node3/sensor/led":
        if msg.payload == "1":
            global sensorled
            sensorled = 1
            GPIO.output(LED_PIN, True)
        elif msg.payload == "0":
            global sensorled
            sensorled = 0
            GPIO.output(LED_PIN, False)
    elif msg.topic == "node/node3/sensor/temperature":
        if msg.payload == "1":
            global sensortemp
            sensortemp = 1
            #sensor temp on
        elif msg.payload == "0":
            global sensortemp
            sensortemp = 0
            #sensor temp off
    elif msg.topic == "node/node3/sensor/humidity":
        if msg.payload == "1":
            global sensorhum
            sensorhum = 1
            #sensor humidity on
        elif msg.payload == "0":
            global sensorhum
            sensorhum = 0
            #sensor humidity off
        

def on_publish(mosq, obj, mid):
    print("DATA :  " + str(mid))


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect(Broker, 1883, 60)
client.loop_start()

while True:
    hasil = sensordht.read()
    if sensortemp=="0":
        client.publish("node/node3/sensor/temperature/status", "SENSOR OFF")
        print("Temp: " + "SENSOR OFF")
    else:
        if hasil.is_valid():
            client.publish("node/node3/sensor/temperature/status", hasil.temperature)
            print("Temp: " + str(hasil.temperature))
        
    if sensorhum=="0":
        client.publish("node/node3/sensor/humidity/status", "SENSOR OFF")
        print("Hum: " + "SENSOR OFF")
    else:
        if hasil.is_valid():
            client.publish("node/node3/sensor/humidity/status", hasil.humidity)
            print("Hum: " + str(hasil.humidity))
        
    if sensorled=="0":
        client.publish("node/node3/sensor/led/status", "SENSOR OFF")
        print("Led: " + "SENSOR OFF")
    else:
        client.publish("node/node3/sensor/led/status", sensorled)
        print("Led: " + str(sensorled))
    time.sleep(5)