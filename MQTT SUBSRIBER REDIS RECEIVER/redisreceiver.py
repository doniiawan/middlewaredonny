import paho.mqtt.client as mqttClient
import time
import redis
import json

r = redis.StrictRedis(host='localhost',port=6379, db=0)

def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to broker")
        global Connected                #Use global variable
        Connected = True                #Signal connection 
    else:
        print("Connection failed")
 
def on_message(client, userdata, message):
    print("Message received: "  + message.payload)
    r.set(message.topic,message.payload)
    

Connected = False
broker_addr="127.0.0.1"
port=1883
#user="coba"
#password="coba"

client=mqttClient.Client("RedisReceiver")
client.on_connect= on_connect
client.on_message= on_message

client.connect(broker_addr,port)

client.loop_start()
while Connected != True:
    time.sleep(0.1)
client.subscribe("node/#")
try:
    while True:
        time.sleep(1)
except KeyboardInterrupt:
    print ("exiting")
    client.disconnect()
    client.loop_stop()
        
