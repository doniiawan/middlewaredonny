#!flask/bin/python
from flask import Flask, request, jsonify, Response
from flask_mqtt import Mqtt
from flask_pymongo import PyMongo
import redis,json


r = redis.StrictRedis(host='localhost',port=6379, db=0)
app = Flask(__name__)
app.config['MQTT_BROKER_URL'] = '127.0.0.1'
app.config['MQTT_BROKER_PORT'] = 1883
app.config['MQTT_USERNAME'] = ''
app.config['MQTT_PASSWORD'] = ''
app.config['MQTT_KEEPALIVE'] = 5
app.config['MQTT_TLS_ENABLED'] = False
app.config['MONGO_DBNAME'] = 'middleware'
mongo = PyMongo(app, config_prefix='MONGO')
mqtt = Mqtt(app)

@app.route('/addnode', methods=['GET','POST'])
def addnode():
    if request.method=='POST':
        content = request.get_json()
        label = content['label']
        topic = content['topic']
        mongo.db.middleware.insert({"_id":content['node'],"sensors":{label:topic}})
	output={'code':'200','message':'Success Adding Node :'+content['node']}
        return jsonify({'result':output})
    elif request.method=='GET':
        output={'code':'404','message':'Error'}
	response = jsonify({'result':output})
	response.status_code=404
        return response

@app.route('/addsensor', methods=['GET','POST'])
def addsensor():
    if request.method=='POST':
        content = request.get_json()
        label = content['label']
        topic = content['topic']
        mongo.db.middleware.update({"_id":content['node']},{"$set":{"sensors."+label:topic}})
	output={'code':'200','message':'Success Adding Sensor : '+label+ ' at ' + content['node'] }
        return jsonify({'result':output})
    elif request.method=='GET':
        output={'code':'404','message':'Error'}
	response = jsonify({'result':output})
	response.status_code=404
        return response
    
@app.route('/node',methods=['GET'])
def shownode():
    platform = mongo.db.middleware
    output = []
    for h in platform.find():
        output.append(h)
    return jsonify({'code':'200','result':output})

@app.route('/node/<name>',methods=['GET'])
def showsensor(name):
    platform = mongo.db.middleware
    h = platform.find_one({'_id':name})
    output = []
    if h:
        output.append(h)
    else:
        output={'code':'404','message':'Node Not Found'}
	response = jsonify({'result':output})
	response.status_code=404
        return response
    return jsonify({'code':'200','result':output})

@app.route('/node/<name>/sensor',methods=['GET'])
def checknodesensor(name):
    if request.method=='GET':
        platform = mongo.db.middleware
        h = platform.find_one({'_id':name})
        output = []
        listsensor=[]    
        status=""
        if h:
            output.append(h)
            dicthasil={}
            dicthasil.update(output[0]['sensors'])
            for key, value in dicthasil.iteritems():

                if r.get("node/"+name+"/"+value+"/status"):
                    data=r.get("node/"+name+"/"+value+"/status")
                    stats={key:data}
                    listsensor.append(dict(stats))
                else:
		    stats={key:'Sensor Not Found'}
		    listsensor.append(dict(stats))
        else:
            output={'code':'404','message':'Node Not Found'}
	    response = jsonify({'result':output})
	    response.status_code=404
            return response
        return jsonify({'code':'200','result':listsensor})

@app.route('/node/<name>/sensor/<sensors>',methods=['GET','POST'])
def pubsubsensor(name, sensors):
    if request.method=='GET':
        platform = mongo.db.middleware
        h = platform.find_one({'_id':name})
        output = {}
        topic="node/"+name+"/sensor/"+sensors+"/status"
        status=""
        if h:
            output.update(h)
            if r.get(topic):
                status = r.get(topic)
            else:
	         output={'code':'404','message':'Sensor Not Found'}
	         response = jsonify({'result':output})
	         response.status_code=404
                 return response
        else:
	    output={'code':'404','message':'Node Not Found'}
	    response = jsonify({'result':output})
	    response.status_code=404
            return response
	output={'code':'200','status':status}
        return jsonify({'result':output})
    elif request.method=='POST':
        content = request.get_json()
        topic="node/"+name+"/sensor/"+sensors
        message=content[sensors]
        mqtt.publish(topic, message)
        output={'code':'200','message':'Publish success','topic':topic,'message':message}    
	return jsonify({'result':output})

#THIS BELOW METHOD IS FOR TESTING PURPOSES ONLY
@app.route('/testing',methods=['GET'])
def testing():
    return jsonify({'id':'node2','led':'0'})
@app.route('/gettest',methods=['GET'])
def gettest():
    status = r.get('node1')
    return status
@app.route('/posttest',methods=['POST'])
def posttest():
    content = request.get_json()
    node = content['node']
    led = content['led']
    r.set(node,led)
    return jsonify({'msg':'success'})
if __name__ == '__main__' :
    app.run(host='0.0.0.0', debug = False, use_reloader=True, threaded = True)
