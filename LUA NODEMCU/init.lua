station_cfg={}
station_cfg.ssid="REST-Middleware"
station_cfg.pwd="zxcvbnm123"

wifi.setmode(wifi.STATION)
wifi.sta.config(station_cfg)
wifiStatusPrev = wifi.STA_IDLE
print("WiFi: Connecting to "..station_cfg.ssid)
wifi.sta.connect()

--wifi reconnect logic timer
tmr.alarm(0, 500, 1, function()
    wifiStatus = wifi.sta.status()
    if wifiStatus ~= wifiStatusPrev then
        print("WiFi: status change "..wifiStatusPrev.."->"..wifiStatus)
    end
    if (wifiStatusPrev ~= 5 and wifiStatus == 5) then 
        print("WiFi: connected")
        print(wifi.sta.getip())
    elseif (wifiStatus ~= 1 and wifiStatus ~= 5) then
      print("WiFi: Reconnecting to "..WIFI_SSID)
      wifi.sta.connect()
    end
    wifiStatusPrev = wifiStatus
end)
dofile("mqtt.lua")