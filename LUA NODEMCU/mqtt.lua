MQTT_BrokerIP = "192.168.1.2"
MQTT_BrokerPort = 1883
MQTT_ClientID = "NODEMCU-1"
--MQTT_Client_user = "user"
--MQTT_Client_password = "password"

led2 = 4
gpio.write(led2, gpio.HIGH)
gpio.mode(led2, gpio.OUTPUT)
d=require('dht11')
d.init(5)

--initialize sensor status = on
sensortemp = 1
sensorhum = 1

m = mqtt.Client(MQTT_ClientID, 120)

m:on("message", function(client, topic, data) 
    print("MQTT: "..topic .. ":" ) 
    if topic=="node/node1/sensor/led" then
        if data ~= nil then
            if data=="1" then
            gpio.write(led2, gpio.LOW)
            elseif data=="0" then
            gpio.write(led2, gpio.HIGH)
            end
        end
    elseif topic=="node/node1/sensor/temperature" then
        if data ~= nil then
            print(data)
            if data=="1" then
            sensortemp=1
            elseif data=="0" then
            sensortemp=0
            end
        end
    elseif topic=="node/node1/sensor/humidity" then    
        if data ~= nil then
            print(data)
            if data=="1" then
            sensorhum=1
            print(data)
            sensorhum=0
            end
        end
    end
end)

m:on("offline", function
(client)
    publishMqtt:stop()
    print("MQTT: offline")
end)

--MQTT reconnect logic timer
reconnMqtt = tmr.create()

reconnMqtt:register(1, tmr.ALARM_SEMI, function (t)
  reconnMqtt:interval(500);
  print("MQTT: trying to connect to "..MQTT_BrokerIP..":"..MQTT_BrokerPort);
  m:close()
  m:connect(MQTT_BrokerIP, MQTT_BrokerPort, 0, 1, function(client) 
    print("MQTT: connected")
    publishMqtt:start()
    print("PUBLISH: STARTED")
    
    print("SUBSRIBE : STARTED")
    m:subscribe("node/node1/sensor/led",0, function(conn) print("subscribe LED success") end)
    m:subscribe("node/node1/sensor/temperature",0, function(conn) print("subscribe TEMP success") end)
    m:subscribe("node/node1/sensor/humidity",0, function(conn) print("subscribe HUM success") end)
 end, function(client, reason) 
    --publishMqtt:stop()
    print("MQTT: connection failed with reason "..reason)
    reconnMqtt:start()
 end)
end)

--MQTT local timestamp publishing timer
publishMqtt = tmr.create()
publishMqtt:register(2, tmr.ALARM_AUTO, function (t)
  publishMqtt:interval(5000);

  ledstatus = 0
  tempstatus = "ON"
  humstatus = "ON"

  if gpio.read(led2) == 0 then
    ledstatus=1
  else
    ledstatus=0
  end
  
  if sensortemp == 0 then
    tempstatus="OFF"
  else
    tempstatus=d.getTemp()
  end

  if sensorhum == 0 then
    humstatus="OFF"
  else
    humstatus=d.getHumidity()
  end

  m:publish("node/node1/sensor/led/status", ledstatus, 0, 0, function(client) print("DATA DIKIRIMKAN : ", ledstatus) end)
  m:publish("node/node1/sensor/temperature/status", tempstatus, 0, 0, function(client) print("DATA DIKIRIMKAN : ", tempstatus) end)
  m:publish("node/node1/sensor/humidity/status", humstatus, 0, 0, function(client) print("DATA DIKIRIMKAN : ", humstatus) end)
  print("PUBLISH DATA")
end)

reconnMqtt:start()
